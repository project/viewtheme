Module: viewtheme.module
File:   README.txt
Author: Nic Ivy (http://drupal.org/user/6194)
CVS:    $Id$

-----------------------------------------------------------------------------
OVERVIEW

  Using views, change the system theme for pages like node/123.

  Use the views module to list nodes, filtering them by taxonomy, content
  type, author, node queue, and other parameters.  Then change the system
  theme if a node is in a view that you created.

  For example, you could use the garland theme for your site but the
  pushbutton theme for pages like node/123 that are authored by
  Administrators.

-----------------------------------------------------------------------------
INSTALLING

  1. Get views.module (http://drupal.org/project/views) and install it.
  2. Install this module in the normal way.
  5. Go to "administer" > "user management" > "access control" to configure
     user permissions.
  6. Go to "administer" > "site building" > "view - theme" to associate themes
     with views.

-----------------------------------------------------------------------------
UNINSTALLING

  1. In Drupal, go to "administer" > "site building" > "modules" to disable
     this module.
  2. After disabling, select the "unistall" tab at the top of the same page.

-----------------------------------------------------------------------------
SUPPORT

  * http://drupal.org/project/viewtheme

-----------------------------------------------------------------------------

